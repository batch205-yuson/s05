class Product {
	constructor(name,price,isActive) {
		this.name = name;
		this.isActive = true;

		if(typeof price === "number"){
			this.price = price;
		}
	};

	archive(){
		this.isActive = false
		return this;
	};

	updatePrice(newPrice){
		if(typeof newPrice !== "number"){
			this.price = undefined;
		} else {
			this.price = newPrice;
		}
		return this;
	};
}


class Cart {
	constructor(contents,totalAmount) {
		this.contents = [];
		this.totalAmount = 0;


		if(typeof totalAmount !== "number"){
			this.totalAmount = undefined;
		} else {
			this.totalAmount = totalAmount;
		}
	};

	addToCart(product,quantity){
		this.contents.push({product,quantity});
		return this;
	};

	showCartContents(){
		console.log(cart)
	};

	clearCartContents(){
		this.contents = [];
		return this;
	};

	computeTotal(){
		let finalPrice = 0;
			this.contents.forEach(content => {
				let price = content.product.price 
				let quant = content.quantity
				let total = price * quant

				finalPrice += total;

			})
			this.totalAmount = finalPrice;
			return this;
	};
}

/*class Customer {
	constructor(email,cart,orders) {
		this.email = email;
		this.cart = {};
		this.orders = [];
	}
}*/




let prodA = new Product("Soap",9.99,true);
let prodB = new Product("Shampoo",12.99,true);
console.log(prodA);
console.log(prodB);

let cart = new Cart([],0);
